package main

import (
	"fmt"

	"github.com/hyperledger/fabric/core/chaincode/shim"
	"github.com/hyperledger/fabric/protos/peer"
)

// SmartContract Define the Smart Contract structure
type SmartContract struct {
}

// Init function
func (s *SmartContract) Init(APIstub shim.ChaincodeStubInterface) peer.Response {
	fmt.Println("Init")
	return shim.Success(nil)
}

// Invoke function
func (s *SmartContract) Invoke(APIstub shim.ChaincodeStubInterface) peer.Response {

	// Retrieve the requested Smart Contract function and arguments
	function, args := APIstub.GetFunctionAndParameters()

	// Route to the appropriate handler function to interact with the ledger appropriately
	fmt.Println(function)
	if function == "addFamilyMember" {
		return addFamilyMember(APIstub, args)
	} else if function == "modFamilyMember" {
		return modFamilyMember(APIstub, args)
	} else if function == "qryFamilyMember" {
		return qryFamilyMember(APIstub, args)
	} else if function == "delFamilyMember" {
		return delFamilyMember(APIstub, args)
	} else if function == "delMedicalRecord" {
		return delMedicalRecord(APIstub, args)
	} else if function == "qryMedicalRecord" {
		return qryMedicalRecord(APIstub, args)
	} else if function == "modMedicalRecord" {
		return modMedicalRecord(APIstub, args)
	} else if function == "addMedicalRecord" {
		return addMedicalRecord(APIstub, args)
	} else if function == "addBodyData" {
		return addBodyData(APIstub, args)
	} else if function == "modBodyData" {
		return modBodyData(APIstub, args)
	} else if function == "qryBodyData" {
		return qryBodyData(APIstub, args)
	} else if function == "delBodyData" {
		return delBodyData(APIstub, args)
	} else if function == "addBloodGlucose" {
		return addBloodGlucose(APIstub, args)
	} else if function == "modBloodGlucose" {
		return modBloodGlucose(APIstub, args)
	} else if function == "qryBloodGlucose" {
		return qryBloodGlucose(APIstub, args)
	} else if function == "delBloodGlucose" {
		return delBloodGlucose(APIstub, args)
	} else if function == "addBloodPressure" {
		return addBloodPressure(APIstub, args)
	} else if function == "modBloodPressure" {
		return modBloodPressure(APIstub, args)
	} else if function == "qryBloodPressure" {
		return qryBloodPressure(APIstub, args)
	} else if function == "delBloodPressure" {
		return delBloodPressure(APIstub, args)
	} else if function == "addPhysicalExaminationRecord" { //添加用户健康检查
		return addPhysicalExaminationRecord(APIstub, args)
	} else if function == "modPhysicalExaminationRecord" { //修改用户健康检查
		return modPhysicalExaminationRecord(APIstub, args)
	} else if function == "qryPhysicalExaminationRecord" { //查询用户健康检查
		return qryPhysicalExaminationRecord(APIstub, args)
	} else if function == "addAbnormalRecord" { //添加用户健康检查异常记录
		return addAbnormalRecord(APIstub, args)
	} else if function == "modAbnormalRecord" { //修改用户健康检查异常记录
		return modAbnormalRecord(APIstub, args)
	} else if function == "qryAbnormalRecord" { //查询用户健康检查异常记录
		return qryAbnormalRecord(APIstub, args)
	} else if function == "oprHealthRecordsAuthority" { //添加、修改健康档案权限
		return oprHealthRecordsAuthority(APIstub, args)
	} else if function == "qryHealthRecordsAuthority" { //查询健康档案权限
		return qryHealthRecordsAuthority(APIstub, args)
	} else if function == "oprSysTagType" { //添加、修改标签类型信息表
		return oprSysTagType(APIstub, args)
	} else if function == "qrySysTagType" { //查询标签类型信息表
		return qrySysTagType(APIstub)
	}

	return shim.Error(errorInvalidFunction)
}

// The main function is only relevant in unit test mode. Only included here for completeness.
func main() {
	err := shim.Start(new(SmartContract))
	if err != nil {
		fmt.Printf("Error creating new Smart Contract: %s", err)
	}
}
