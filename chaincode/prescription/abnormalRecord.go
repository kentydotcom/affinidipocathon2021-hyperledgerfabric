/*
	用户健康检查异常记录链码
*/

package main

import (
	"fmt"

	"github.com/hyperledger/fabric/core/chaincode/shim"
	"github.com/hyperledger/fabric/protos/peer"
)

/*
添加用户健康检查异常记录
	arg1: account_no
	arg2: member_id
	arg3: yyyyMMdd
	arg4: abnormal_record_id
	arg5: json data
*/
func addAbnormalRecord(stub shim.ChaincodeStubInterface, args []string) peer.Response {
	fmt.Println("Function [ addAbnormalRecord ] ")

	if len(args) != 5 {
		return shim.Error("Incorrect number of arguments. Expecting 5")
	}
	if len(args[0]) <= 0 {
		return shim.Error("1st argument must be a non-empty string.Expecting String[account_no]")
	}
	if len(args[1]) <= 0 {
		return shim.Error("2nd argument must be a non-empty string.Expecting String[member_id]")
	}
	if len(args[2]) <= 0 {
		return shim.Error("3rd argument must be a non-empty string.Expecting DateString[yyyyMMdd]")
	}
	if len(args[3]) <= 0 {
		return shim.Error("4nd argument must be a non-empty string.Expecting String[abnormal_record_id]")
	}
	if len(args[4]) <= 0 {
		return shim.Error("5rd argument must be a non-empty string.Expecting JsonString[AbnormalRecord JSON]")
	}

	keyName := "abnormal_record_"
	keyIndex, err := stub.CreateCompositeKey(keyName, []string{args[0], args[1], args[2], args[3]})
	if err != nil {
		return shim.Error("Create Composite Key Failed :" + err.Error())
	}

	err = stub.PutState(keyIndex, []byte(args[4]))
	if err != nil {
		return shim.Error("modAbnormalRecord Error. " + err.Error())
	}

	return shim.Success(nil)
}

/*
修改用户健康检查异常记录
	arg1: account_no
	arg2: member_id
	arg3: yyyyMMdd
	arg4: abnormal_record_id
	arg5: json data
*/
func modAbnormalRecord(stub shim.ChaincodeStubInterface, args []string) peer.Response {
	fmt.Println("Function [ modAbnormalRecord ] ")

	if len(args) != 5 {
		return shim.Error("Incorrect number of arguments. Expecting 5")
	}
	if len(args[0]) <= 0 {
		return shim.Error("1st argument must be a non-empty string.Expecting String[account_no]")
	}
	if len(args[1]) <= 0 {
		return shim.Error("2nd argument must be a non-empty string.Expecting String[member_id]")
	}
	if len(args[2]) <= 0 {
		return shim.Error("3rd argument must be a non-empty string.Expecting DateString[yyyyMMdd]")
	}
	if len(args[3]) <= 0 {
		return shim.Error("4nd argument must be a non-empty string.Expecting String[abnormal_record_id]")
	}
	if len(args[4]) <= 0 {
		return shim.Error("5rd argument must be a non-empty string.Expecting JsonString[AbnormalRecord JSON]")
	}

	keyName := "abnormal_record_"
	keyIndex, err := stub.CreateCompositeKey(keyName, []string{args[0], args[1], args[2], args[3]})
	if err != nil {
		return shim.Error("Create Composite Key Failed :" + err.Error())
	}

	err = stub.PutState(keyIndex, []byte(args[4]))
	if err != nil {
		return shim.Error("modAbnormalRecord Error. " + err.Error())
	}

	return shim.Success(nil)
}

/*
查询用户健康检查异常记录
	arg1: account_no
	arg2: member_id
	arg3: yyyyMMdd
	arg4: abnormal_record_id
*/
func qryAbnormalRecord(stub shim.ChaincodeStubInterface, args []string) peer.Response {
	fmt.Println("Function [ qryAbnormalRecord ] ")

	if len(args) != 4 {
		return shim.Error("Incorrect number of arguments. Expecting 4")
	}
	if len(args[0]) <= 0 {
		return shim.Error("1st argument must be a non-empty string.Expecting String[account_no]")
	}
	if len(args[1]) <= 0 {
		return shim.Error("2nd argument must be a non-empty string.Expecting String[member_id]")
	}
	if len(args[2]) <= 0 {
		return shim.Error("3rd argument must be a non-empty string.Expecting DateString[yyyyMMdd]")
	}
	if len(args[3]) <= 0 {
		return shim.Error("4nd argument must be a non-empty string.Expecting String[abnormal_record_id]")
	}

	keyName := "abnormal_record_"
	keyIndex, err := stub.CreateCompositeKey(keyName, args)
	if err != nil {
		return shim.Error("Create Composite Key Failed :" + err.Error())
	}

	result, err := stub.GetState(keyIndex)
	if err != nil {
		return shim.Error("qryAbnormalRecord Error." + err.Error())
	}
	if result == nil {
		return shim.Error("Abnormal Record Not Exist.")
	}

	fmt.Printf("qryAbnormalRecord keyIndex[%s], result[%s]\n", keyIndex, string(result[:]))

	return shim.Success(result)
}
