package main

import (
	"encoding/json"
	"fmt"

	"github.com/hyperledger/fabric/core/chaincode/shim"
	"github.com/hyperledger/fabric/protos/peer"
)

/*	添加、修改用户健康档案权限
	arg1: account_no
	arg2: member_id
	arg3:[{archives_power:1},{sub_health:2},{allergy:1},{disease:1},{physical:1},{health_service:1},{health_inspection:1}]
*/
func oprHealthRecordsAuthority(stub shim.ChaincodeStubInterface, args []string) peer.Response {
	fmt.Println("Function [ oprHealthRecordsAuthority ] ")

	if len(args) != 3 {
		return shim.Error("Incorrect number of arguments. Expecting 3")
	}
	if len(args[0]) <= 0 {
		return shim.Error("1st argument must be a non-empty string")
	}
	if len(args[1]) <= 0 {
		return shim.Error("2nd argument must be a non-empty string")
	}
	if len(args[2]) <= 0 {
		return shim.Error("3rd argument must be a non-empty string")
	}

	keyVal := fmt.Sprintf("health_records_authority_%s_%s", args[0], args[1])

	// 根据keyVal获取区块链中的值
	result, err := stub.GetState(keyVal)
	if err != nil {
		shim.Error("oprHealthRecordsAuthority:GetState Error. " + err.Error())
	}

	// 将区块链中的result字符串解析成map
	var mapResult map[string]interface{}
	if err := json.Unmarshal(result, &mapResult); err != nil {
		shim.Error("oprHealthRecordsAuthority:Convert Result String Error" + err.Error())
	}
	// 将参数中的中的args[3]字符串解析成map
	var mapParameter map[string]interface{}
	if err := json.Unmarshal([]byte(args[2]), &mapParameter); err != nil {
		shim.Error("oprHealthRecordsAuthority:Convert Parameter String Error" + err.Error())
	}
	//Func: 更新mapResult中 key 与 keyParameter 相等的值
	//		- 在mapResult中与keyParameter相同的key存在,直接进行更新;
	//      - 在mapResult中与keyParameter相同的key不存在,进行添加;
	for keyParameter, valParameter := range mapParameter {
		mapResult[keyParameter] = valParameter
	}

	//将mapResult转换成json字符串
	jsonStr, err := json.Marshal(mapResult)

	//更新区块链中的值
	err = stub.PutState(keyVal, []byte(jsonStr))
	if err != nil {
		shim.Error("oprHealthRecordsAuthority:PutState Error. " + err.Error())
	}

	return shim.Success(nil)
}

/*	查询用户健康档案权限
	arg1: account_no
	arg2: member_id
*/
func qryHealthRecordsAuthority(stub shim.ChaincodeStubInterface, args []string) peer.Response {
	fmt.Println("Function [ qryHealthRecordsAuthority ] ")

	if len(args) != 2 {
		return shim.Error("Incorrect number of arguments. Expecting 3")
	}
	if len(args[0]) <= 0 {
		return shim.Error("1st argument must be a non-empty string")
	}
	if len(args[1]) <= 0 {
		return shim.Error("2nd argument must be a non-empty string")
	}

	keyVal := fmt.Sprintf("qryHealthRecordsAuthority_%s_%s_%s", args[0], args[1])

	result, err := stub.GetState(keyVal)
	if err != nil {
		return shim.Error("qryHealthRecordsAuthority Error. " + err.Error())
	}

	fmt.Printf("health_records_authority keyVal[%s], result[%s]", keyVal, result)

	return shim.Success([]byte(result))
}
