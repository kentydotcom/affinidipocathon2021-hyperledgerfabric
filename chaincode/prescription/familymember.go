package main

import (
	"fmt"

	"github.com/hyperledger/fabric/core/chaincode/shim"
	"github.com/hyperledger/fabric/protos/peer"
)

func addFamilyMember(stub shim.ChaincodeStubInterface, args []string) peer.Response {
	if len(args) != 3 {
		return shim.Error(errorIncorrectParamNum + " Expecting 3")
	}

	fmt.Println("args=", args)
	accountNo := args[0]
	if accountNo == "" {
		return shim.Error(errorMissParamNum + " Expect argument account_no")
	}

	memberId := args[1]
	if memberId == "" {
		return shim.Error(errorMissParamNum + "Expect argument member_id")
	}

	key := "family_member_" + accountNo + "_" + memberId
	ret, err := stub.GetState(key)
	if err != nil {
		return shim.Error(errorBlockchainError + err.Error())
	} else if ret != nil {
		return shim.Error(errorDataAlreadyExist)
	}

	err = stub.PutState(key, []byte(args[2]))
	if err != nil {
		return shim.Error(errorBlockchainError + err.Error())
	}
	return shim.Success([]byte(key))
}

func modFamilyMember(stub shim.ChaincodeStubInterface, args []string) peer.Response {
	if len(args) != 3 {
		return shim.Error(errorIncorrectParamNum + " Expecting 3")
	}

	fmt.Println("args=", args)
	accountNo := args[0]
	if accountNo == "" {
		return shim.Error(errorMissParamNum + "Expect argument account_no")
	}

	memberId := args[1]
	if memberId == "" {
		return shim.Error(errorMissParamNum + "Expect argument member_id")
	}

	key := "family_member_" + accountNo + "_" + memberId
	ret, err := stub.GetState(key)
	if err != nil {
		return shim.Error(errorBlockchainError + err.Error())
	} else if ret == nil {
		return shim.Error(errorDataNotExist)
	}

	err = stub.PutState(key, []byte(args[2]))
	if err != nil {
		return shim.Error(errorBlockchainError + err.Error())
	}
	return shim.Success([]byte(key))
}

func qryFamilyMember(stub shim.ChaincodeStubInterface, args []string) peer.Response {
	if len(args) != 2 {
		return shim.Error(errorIncorrectParamNum + " Expecting 2")
	}

	fmt.Println("args=", args)
	accountNo := args[0]
	if accountNo == "" {
		return shim.Error(errorMissParamNum + "Expect argument account_no")
	}

	memberId := args[1]
	if memberId == "" {
		return shim.Error(errorMissParamNum + "Expect argument member_id")
	}

	key := "family_member_" + accountNo + "_" + memberId
	ret, err := stub.GetState(key)
	if err != nil {
		return shim.Error(errorBlockchainError + err.Error())
	} else if ret == nil {
		return shim.Error(errorDataNotExist)
	} else {
		return shim.Success(ret)
	}
}

func delFamilyMember(stub shim.ChaincodeStubInterface, args []string) peer.Response {
	if len(args) != 2 {
		return shim.Error(errorIncorrectParamNum + " Expecting 2")
	}

	fmt.Println("args=", args)
	accountNo := args[0]
	if accountNo == "" {
		return shim.Error(errorMissParamNum + "Expect argument account_no")
	}

	memberId := args[1]
	if memberId == "" {
		return shim.Error(errorMissParamNum + "Expect argument member_id")
	}

	key := "family_member_" + accountNo + "_" + memberId
	err := stub.DelState(key)
	if err != nil {
		return shim.Error(errorBlockchainError + err.Error())
	} else {
		return shim.Success(nil)
	}
}