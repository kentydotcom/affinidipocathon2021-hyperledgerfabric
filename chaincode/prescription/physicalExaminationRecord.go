/**
用户健康检查链码
*/

package main

import (
	"fmt"

	"github.com/hyperledger/fabric/core/chaincode/shim"
	"github.com/hyperledger/fabric/protos/peer"
)

/*
 添加用户健康检查
	 arg1: account_no
	 arg2: member_id
	 arg3: yyyyMMdd
	 arg4: json数据
*/
func addPhysicalExaminationRecord(stub shim.ChaincodeStubInterface, args []string) peer.Response {
	fmt.Println("Function [ addPhysicalExaminationRecord ] ")

	if len(args) != 4 {
		return shim.Error("Incorrect number of arguments. Expecting 4")
	}
	if len(args[0]) <= 0 {
		return shim.Error("1st argument must be a non-empty string.Expecting String[account_no]")
	}
	if len(args[1]) <= 0 {
		return shim.Error("2nd argument must be a non-empty string.Expecting String[member_id]")
	}
	if len(args[2]) <= 0 {
		return shim.Error("3rd argument must be a non-empty string.Expecting DateString[yyyyMMdd]")
	}
	if len(args[3]) <= 0 {
		return shim.Error("4th argument must be a non-empty string.Expecting JsonString[PhysicalExaminationRecord JSON]")
	}

	keyName := "physical_examination_record_"
	keyIndex, err := stub.CreateCompositeKey(keyName, []string{args[0], args[1], args[2]})
	if err != nil {
		return shim.Error("Create Composite Key Failed :" + err.Error())
	}

	err = stub.PutState(keyIndex, []byte(args[3]))
	if err != nil {
		shim.Error("addPhysicalExaminationRecord Error. " + err.Error())
	}
	fmt.Printf(" [addPhysicalExaminationRecord] - { %s:%s } Successful. \n", keyIndex, args[3])

	return shim.Success(nil)
}

/*
 修改用户健康检查记录
	 arg1: account_no
	 arg2: member_id
	 arg3: yyyyMMdd
	 arg4: json数据
*/
func modPhysicalExaminationRecord(stub shim.ChaincodeStubInterface, args []string) peer.Response {
	fmt.Println("Function [ modPhysicalExaminationRecord ] ")

	if len(args) != 4 {
		return shim.Error("Incorrect number of arguments. Expecting 4")
	}
	if len(args[0]) <= 0 {
		return shim.Error("1st argument must be a non-empty string.Expecting String[account_no]")
	}
	if len(args[1]) <= 0 {
		return shim.Error("2nd argument must be a non-empty string.Expecting String[member_id]")
	}
	if len(args[2]) <= 0 {
		return shim.Error("3rd argument must be a non-empty string.Expecting DateString[yyyyMMdd]")
	}
	if len(args[3]) <= 0 {
		return shim.Error("4th argument must be a non-empty string.Expecting JsonString[PhysicalExaminationRecord JSON]")
	}

	keyName := "physical_examination_record_"
	keyIndex, err := stub.CreateCompositeKey(keyName, []string{args[0], args[1], args[2]})
	if err != nil {
		return shim.Error("Create Composite Key Failed :" + err.Error())
	}

	err = stub.PutState(keyIndex, []byte(args[3]))
	if err != nil {
		shim.Error("modPhysicalExaminationRecord Error. " + err.Error())
	}
	fmt.Printf(" [modPhysicalExaminationRecord] - { %s:%s } Successful. \n", keyIndex, args[3])

	return shim.Success(nil)
}

/*
 查询用户健康检查
	 arg1: account_no
	 arg2: member_id
	 arg3: yyyyMMdd
*/
func qryPhysicalExaminationRecord(stub shim.ChaincodeStubInterface, args []string) peer.Response {
	fmt.Println("Function [ qryPhysicalExaminationRecord ] ")

	if len(args) != 3 {
		return shim.Error("Incorrect number of arguments. Expecting 3")
	}
	if len(args[0]) <= 0 {
		return shim.Error("1st argument must be a non-empty string.Expecting String[account_no]")
	}
	if len(args[1]) <= 0 {
		return shim.Error("2nd argument must be a non-empty string.Expecting String[member_id]")
	}
	if len(args[2]) <= 0 {
		return shim.Error("3rd argument must be a non-empty string.Expecting String[yyyyMMdd]")
	}

	keyName := "physical_examination_record_"
	keyIndex, err := stub.CreateCompositeKey(keyName, args)

	result, err := stub.GetState(keyIndex)
	if err != nil {
		return shim.Error("qryPhysicalExaminationRecord Error. " + err.Error())
	}
	if result == nil {
		return shim.Error("Physical Examination Record Not Exist.")
	}

	fmt.Printf("qryPhysicalExaminationRecord keyIndex[%s], Result[%s]\n", keyIndex, string(result[:]))

	return shim.Success(result)
}
