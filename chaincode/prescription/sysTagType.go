package main

import (
	"fmt"

	"github.com/hyperledger/fabric/core/chaincode/shim"
	"github.com/hyperledger/fabric/protos/peer"
)

/*	添加、修改标签类型信息表
	arg1:[{type_id:1,type_name:1,tag_level:1,parent_id:1},{type_id:1,type_name:1,tag_level:1,parent_id:1}]
*/
func oprSysTagType(stub shim.ChaincodeStubInterface, args []string) peer.Response {
	fmt.Println("Function [ oprSysTagType ] ")
	if len(args) != 1 {
		return shim.Error("Incorrect number of arguments. Expecting 1")
	}
	if len(args[0]) <= 0 {
		return shim.Error("1st argument must be a non-empty string")
	}

	keyVal := fmt.Sprintf("SysTagType")

	err := stub.PutState(keyVal, []byte(args[0]))
	if err != nil {
		shim.Error("oprSysTagType:PuState Error." + err.Error())
	}
	return shim.Success(nil)
}

/*
	查询标签类型信息表
*/
func qrySysTagType(stub shim.ChaincodeStubInterface) peer.Response {
	fmt.Println("Function [ qrySysTagType ]")

	keyVal := fmt.Sprintf("qrySysTagType")

	result, err := stub.GetState(keyVal)
	if err != nil {
		return shim.Error("qrySysTagType Error. " + err.Error())
	}
	fmt.Printf("SysTagType keyVal[%s], result[%s]", keyVal, result)

	return shim.Success([]byte(result))
}
