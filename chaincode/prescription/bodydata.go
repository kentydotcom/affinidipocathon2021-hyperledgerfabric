package main

import (
	"fmt"
	"strings"

	"github.com/hyperledger/fabric/core/chaincode/shim"
	pb "github.com/hyperledger/fabric/protos/peer"
)

func addBodyData(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	fmt.Println("addBodyData began")
	if len(args) != 5 {
		return shim.Error("Incorrect number of arguments. Expecting 5")
	}
	var arg1 string = args[0]
	if strings.TrimSpace(arg1) == "" {
		return shim.Error("AccountNo missing ")
	}
	var arg2 string = args[1]
	if strings.TrimSpace(arg2) == "" {
		return shim.Error("MemberId missing ")
	}
	var arg3 string = args[2]
	if strings.TrimSpace(arg3) == "" {
		return shim.Error("GaugeData missing ")
	}
	var arg4 string = args[3]
	if strings.TrimSpace(arg4) == "" {
		return shim.Error("GaugeTime missing ")
	}
	var arg5 string = args[4]
	if strings.TrimSpace(arg5) == "" {
		return shim.Error("Body missing ")
	}
	indexName := "body_data_accountNo_memberId_gaugeData_gaugeTime"
	indexKey, errk := stub.CreateCompositeKey(indexName, []string{arg1, arg2, arg3, arg4})
	if errk != nil {
		return shim.Error(errk.Error())
	}
	data, err := stub.GetState(indexKey)
	if err != nil {
		return shim.Error(err.Error())
	}
	if data != nil {
		return shim.Error("data exist")
	}
	errP := stub.PutState(indexKey, []byte(arg5))
	if errP != nil {
		return shim.Error(errP.Error())
	}
	fmt.Println("addBodyData success")
	return shim.Success(nil)
}

func delBodyData(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	fmt.Println("delBodyData began")
	if len(args) != 4 {
		return shim.Error("Incorrect number of arguments. Expecting 4")
	}
	var arg1 string = args[0]
	if strings.TrimSpace(arg1) == "" {
		return shim.Error("AccountNo missing ")
	}
	var arg2 string = args[1]
	if strings.TrimSpace(arg2) == "" {
		return shim.Error("MemberId missing ")
	}
	var arg3 string = args[2]
	if strings.TrimSpace(arg3) == "" {
		return shim.Error("GaugeData missing ")
	}
	var arg4 string = args[3]
	if strings.TrimSpace(arg4) == "" {
		return shim.Error("GaugeTime missing ")
	}
	indexName := "body_data_accountNo_memberId_gaugeData_gaugeTime"
	indexKey, errk := stub.CreateCompositeKey(indexName, []string{arg1, arg2, arg3, arg4})
	if errk != nil {
		return shim.Error(errk.Error())
	}
	data, err := stub.GetState(indexKey)
	if err != nil {
		return shim.Error(err.Error())
	}
	if data == nil {
		return shim.Error("data not exist")
	}
	errP := stub.DelState(indexKey)
	if errP != nil {
		return shim.Error(errP.Error())
	}
	fmt.Println("delBodyData success")
	return shim.Success(nil)
}

func modBodyData(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	fmt.Println("modBodyData began")
	if len(args) != 5 {
		return shim.Error("Incorrect number of arguments. Expecting 5")
	}
	var arg1 string = args[0]
	if strings.TrimSpace(arg1) == "" {
		return shim.Error("AccountNo missing ")
	}
	var arg2 string = args[1]
	if strings.TrimSpace(arg2) == "" {
		return shim.Error("MemberId missing ")
	}
	var arg3 string = args[2]
	if strings.TrimSpace(arg3) == "" {
		return shim.Error("GaugeData missing ")
	}
	var arg4 string = args[3]
	if strings.TrimSpace(arg4) == "" {
		return shim.Error("GaugeTime missing ")
	}
	var arg5 string = args[4]
	if strings.TrimSpace(arg5) == "" {
		return shim.Error("Body missing ")
	}
	indexName := "body_data_accountNo_memberId_gaugeData_gaugeTime"
	indexKey, errk := stub.CreateCompositeKey(indexName, []string{arg1, arg2, arg3, arg4})
	if errk != nil {
		return shim.Error(errk.Error())
	}
	data, err := stub.GetState(indexKey)
	if err != nil {
		return shim.Error(err.Error())
	}
	if data == nil {
		return shim.Error("data not exist")
	}
	errP := stub.PutState(indexKey, []byte(arg5))
	if errP != nil {
		return shim.Error(errP.Error())
	}
	fmt.Println("modBodyData success")
	return shim.Success(nil)
}
func qryBodyData(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	fmt.Println("qryBodyData began")
	if len(args) != 4 {
		return shim.Error("Incorrect number of arguments. Expecting 4")
	}
	var arg1 string = args[0]
	if strings.TrimSpace(arg1) == "" {
		return shim.Error("AccountNo missing ")
	}
	var arg2 string = args[1]
	if strings.TrimSpace(arg2) == "" {
		return shim.Error("MemberId missing ")
	}
	var arg3 string = args[2]
	if strings.TrimSpace(arg3) == "" {
		return shim.Error("GaugeData missing ")
	}
	var arg4 string = args[3]
	if strings.TrimSpace(arg4) == "" {
		return shim.Error("GaugeTime missing ")
	}
	indexName := "body_data_accountNo_memberId_gaugeData_gaugeTime"
	indexKey, errk := stub.CreateCompositeKey(indexName, []string{arg1, arg2, arg3, arg4})
	if errk != nil {
		return shim.Error(errk.Error())
	}
	data, err := stub.GetState(indexKey)
	if err != nil {
		return shim.Error(err.Error())
	}
	fmt.Println("qryBodyData success")
	return shim.Success(data)
}

func addBloodGlucose(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	fmt.Println("addBloodGlucose began")
	if len(args) != 5 {
		return shim.Error("Incorrect number of arguments. Expecting 5")
	}
	var arg1 string = args[0]
	if strings.TrimSpace(arg1) == "" {
		return shim.Error("AccountNo missing ")
	}
	var arg2 string = args[1]
	if strings.TrimSpace(arg2) == "" {
		return shim.Error("MemberId missing ")
	}
	var arg3 string = args[2]
	if strings.TrimSpace(arg3) == "" {
		return shim.Error("GaugeData missing ")
	}
	var arg4 string = args[3]
	if strings.TrimSpace(arg4) == "" {
		return shim.Error("Period missing ")
	}
	var arg5 string = args[4]
	if strings.TrimSpace(arg5) == "" {
		return shim.Error("Body missing ")
	}
	indexName := "blood_glucose_accountNo_memberId_gaugeData_period"
	indexKey, errk := stub.CreateCompositeKey(indexName, []string{arg1, arg2, arg3, arg4})
	if errk != nil {
		return shim.Error(errk.Error())
	}

	data, err := stub.GetState(indexKey)
	if err != nil {
		return shim.Error(err.Error())
	}
	if data != nil {
		return shim.Error("data exist")
	}
	errP := stub.PutState(indexKey, []byte(arg5))
	if errP != nil {
		return shim.Error(errP.Error())
	}
	fmt.Println("addBloodGlucose success")
	return shim.Success(nil)
}

func delBloodGlucose(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	fmt.Println("delBloodGlucose began")
	if len(args) != 4 {
		return shim.Error("Incorrect number of arguments. Expecting 4")
	}
	var arg1 string = args[0]
	if strings.TrimSpace(arg1) == "" {
		return shim.Error("AccountNo missing ")
	}
	var arg2 string = args[1]
	if strings.TrimSpace(arg2) == "" {
		return shim.Error("MemberId missing ")
	}
	var arg3 string = args[2]
	if strings.TrimSpace(arg3) == "" {
		return shim.Error("GaugeData missing ")
	}
	var arg4 string = args[3]
	if strings.TrimSpace(arg4) == "" {
		return shim.Error("Period missing ")
	}
	indexName := "blood_glucose_accountNo_memberId_gaugeData_period"
	indexKey, errk := stub.CreateCompositeKey(indexName, []string{arg1, arg2, arg3, arg4})
	if errk != nil {
		return shim.Error(errk.Error())
	}
	data, err := stub.GetState(indexKey)
	if err != nil {
		return shim.Error(err.Error())
	}
	if data == nil {
		return shim.Error("data not exist")
	}
	errP := stub.DelState(indexKey)
	if errP != nil {
		return shim.Error(errP.Error())
	}
	fmt.Println("delBloodGlucose success")
	return shim.Success(nil)
}

func modBloodGlucose(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	fmt.Println("modBloodGlucose began")
	if len(args) != 5 {
		return shim.Error("Incorrect number of arguments. Expecting 5")
	}
	var arg1 string = args[0]
	if strings.TrimSpace(arg1) == "" {
		return shim.Error("AccountNo missing ")
	}
	var arg2 string = args[1]
	if strings.TrimSpace(arg2) == "" {
		return shim.Error("MemberId missing ")
	}
	var arg3 string = args[2]
	if strings.TrimSpace(arg3) == "" {
		return shim.Error("GaugeData missing ")
	}
	var arg4 string = args[3]
	if strings.TrimSpace(arg4) == "" {
		return shim.Error("Period missing ")
	}
	var arg5 string = args[4]
	if strings.TrimSpace(arg5) == "" {
		return shim.Error("Body missing ")
	}
	indexName := "blood_glucose_accountNo_memberId_gaugeData_period"
	indexKey, errk := stub.CreateCompositeKey(indexName, []string{arg1, arg2, arg3, arg4})
	if errk != nil {
		return shim.Error(errk.Error())
	}

	data, err := stub.GetState(indexKey)
	if err != nil {
		return shim.Error(err.Error())
	}
	if data == nil {
		return shim.Error("data not exist")
	}
	errP := stub.PutState(indexKey, []byte(arg5))
	if errP != nil {
		return shim.Error(errP.Error())
	}
	fmt.Println("modBloodGlucose success")
	return shim.Success(nil)
}
func qryBloodGlucose(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	fmt.Println("qryBloodGlucose began")
	if len(args) != 4 {
		return shim.Error("Incorrect number of arguments. Expecting 5")
	}
	var arg1 string = args[0]
	if strings.TrimSpace(arg1) == "" {
		return shim.Error("AccountNo missing ")
	}
	var arg2 string = args[1]
	if strings.TrimSpace(arg2) == "" {
		return shim.Error("MemberId missing ")
	}
	var arg3 string = args[2]
	if strings.TrimSpace(arg3) == "" {
		return shim.Error("GaugeData missing ")
	}
	var arg4 string = args[3]
	if strings.TrimSpace(arg4) == "" {
		return shim.Error("Period missing ")
	}
	indexName := "blood_glucose_accountNo_memberId_gaugeData_period"
	indexKey, errk := stub.CreateCompositeKey(indexName, []string{arg1, arg2, arg3, arg4})
	if errk != nil {
		return shim.Error(errk.Error())
	}

	data, err := stub.GetState(indexKey)
	if err != nil {
		return shim.Error(err.Error())
	}
	fmt.Println("qryBloodGlucose success")
	return shim.Success(data)
}

func addBloodPressure(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	fmt.Println("addBloodPressure began")
	if len(args) != 5 {
		return shim.Error("Incorrect number of arguments. Expecting 5")
	}
	var arg1 string = args[0]
	if strings.TrimSpace(arg1) == "" {
		return shim.Error("AccountNo missing ")
	}
	var arg2 string = args[1]
	if strings.TrimSpace(arg2) == "" {
		return shim.Error("MemberId missing ")
	}
	var arg3 string = args[2]
	if strings.TrimSpace(arg3) == "" {
		return shim.Error("GaugeData missing ")
	}
	var arg4 string = args[3]
	if strings.TrimSpace(arg4) == "" {
		return shim.Error("GaugeTime missing ")
	}
	var arg5 string = args[4]
	if strings.TrimSpace(arg5) == "" {
		return shim.Error("Body missing ")
	}
	indexName := "blood_pressure_accountNo_memberId_gaugeData_period"
	indexKey, errk := stub.CreateCompositeKey(indexName, []string{arg1, arg2, arg3, arg4})
	if errk != nil {
		return shim.Error(errk.Error())
	}

	data, err := stub.GetState(indexKey)
	if err != nil {
		return shim.Error(err.Error())
	}
	if data != nil {
		return shim.Error("data exist")
	}
	errP := stub.PutState(indexKey, []byte(arg5))
	if errP != nil {
		return shim.Error(errP.Error())
	}
	fmt.Println("addBloodPressure success")
	return shim.Success(nil)
}

func delBloodPressure(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	fmt.Println("delBloodPressure began")
	if len(args) != 4 {
		return shim.Error("Incorrect number of arguments. Expecting 4")
	}
	var arg1 string = args[0]
	if strings.TrimSpace(arg1) == "" {
		return shim.Error("AccountNo missing ")
	}
	var arg2 string = args[1]
	if strings.TrimSpace(arg2) == "" {
		return shim.Error("MemberId missing ")
	}
	var arg3 string = args[2]
	if strings.TrimSpace(arg3) == "" {
		return shim.Error("GaugeData missing ")
	}
	var arg4 string = args[3]
	if strings.TrimSpace(arg4) == "" {
		return shim.Error("GaugeTime missing ")
	}
	indexName := "blood_pressure_accountNo_memberId_gaugeData_period"
	indexKey, errk := stub.CreateCompositeKey(indexName, []string{arg1, arg2, arg3, arg4})
	if errk != nil {
		return shim.Error(errk.Error())
	}

	data, err := stub.GetState(indexKey)
	if err != nil {
		return shim.Error(err.Error())
	}
	if data == nil {
		return shim.Error("data not exist")
	}
	errP := stub.DelState(indexKey)
	if errP != nil {
		return shim.Error(errP.Error())
	}
	fmt.Println("delBloodPressure success")
	return shim.Success(nil)
}

func modBloodPressure(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	fmt.Println("modBloodPressure began")
	if len(args) != 5 {
		return shim.Error("Incorrect number of arguments. Expecting 5")
	}
	var arg1 string = args[0]
	if strings.TrimSpace(arg1) == "" {
		return shim.Error("AccountNo missing ")
	}
	var arg2 string = args[1]
	if strings.TrimSpace(arg2) == "" {
		return shim.Error("MemberId missing ")
	}
	var arg3 string = args[2]
	if strings.TrimSpace(arg3) == "" {
		return shim.Error("GaugeData missing ")
	}
	var arg4 string = args[3]
	if strings.TrimSpace(arg4) == "" {
		return shim.Error("GaugeTime missing ")
	}
	var arg5 string = args[4]
	if strings.TrimSpace(arg5) == "" {
		return shim.Error("Body missing ")
	}
	indexName := "blood_pressure_accountNo_memberId_gaugeData_period"
	indexKey, errk := stub.CreateCompositeKey(indexName, []string{arg1, arg2, arg3, arg4})
	if errk != nil {
		return shim.Error(errk.Error())
	}

	data, err := stub.GetState(indexKey)
	if err != nil {
		return shim.Error(err.Error())
	}
	if data == nil {
		return shim.Error("data not exist")
	}
	errP := stub.PutState(indexKey, []byte(arg5))
	if errP != nil {
		return shim.Error(errP.Error())
	}
	fmt.Println("modBloodPressure success")
	return shim.Success(nil)
}

func qryBloodPressure(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	fmt.Println("qryBloodPressure began")
	if len(args) != 4 {
		return shim.Error("Incorrect number of arguments. Expecting 5")
	}
	var arg1 string = args[0]
	if strings.TrimSpace(arg1) == "" {
		return shim.Error("AccountNo missing ")
	}
	var arg2 string = args[1]
	if strings.TrimSpace(arg2) == "" {
		return shim.Error("MemberId missing ")
	}
	var arg3 string = args[2]
	if strings.TrimSpace(arg3) == "" {
		return shim.Error("GaugeData missing ")
	}
	var arg4 string = args[3]
	if strings.TrimSpace(arg4) == "" {
		return shim.Error("GaugeTime missing ")
	}
	indexName := "blood_pressure_accountNo_memberId_gaugeData_period"
	indexKey, errk := stub.CreateCompositeKey(indexName, []string{arg1, arg2, arg3, arg4})
	if errk != nil {
		return shim.Error(errk.Error())
	}

	data, err := stub.GetState(indexKey)
	if err != nil {
		return shim.Error(err.Error())
	}
	fmt.Println("qryBloodPressure success")
	return shim.Success(data)
}
