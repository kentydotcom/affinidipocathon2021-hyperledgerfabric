package main

import (
	"fmt"

	"github.com/hyperledger/fabric/core/chaincode/shim"
	pb "github.com/hyperledger/fabric/protos/peer"
)

/**
	添加诊疗记录 medical_record

	arg1: account_no
	arg2: member_id
	arg3: yyyyMMdd
	arg4: clinic_type
	arg5: clinic_hospital
	arg6: clinic_department
	arg7: struct
 */
func addMedicalRecord(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	if len(args) != 7 {
		return shim.Error("参数个数不匹配")
	} else if len(args[0]) <= 0 {
		return shim.Error("参数错误")
	} else if len(args[1]) <= 0 {
		return shim.Error("参数错误")
	} else if len(args[2]) <= 0 {
		return shim.Error("参数错误")
	} else if len(args[3]) <= 0 {
		return shim.Error("参数错误")
	} else if len(args[4]) <= 0 {
		return shim.Error("参数错误")
	} else if len(args[5]) <= 0 {
		return shim.Error("参数错误")
	} else if len(args[6]) <= 0 {
		return shim.Error("参数错误")
	}

	var err error

	key := fmt.Sprintf("medical_record_s%_s%_s%_s%_s%_s%", args[0], args[1], args[2], args[3], args[4], args[5])
	value := args[6]
	err = stub.PutState(key, []byte(value))
	if err != nil {
		return shim.Error(err.Error())
	}
	fmt.Println("=========================add success!!!!===========================")
	return shim.Success(nil)
}

/**
	删除诊疗记录

	arg1: account_no
	arg2: member_id
	arg3: yyyyMMdd
	arg4: clinic_type
	arg5: clinic_hospital
	arg6: clinic_department
 */
func delMedicalRecord(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	if len(args) != 6 {
		return shim.Error("参数个数不匹配")
	} else if len(args[0]) <= 0 {
		return shim.Error("参数错误")
	} else if len(args[1]) <= 0 {
		return shim.Error("参数错误")
	} else if len(args[2]) <= 0 {
		return shim.Error("参数错误")
	} else if len(args[3]) <= 0 {
		return shim.Error("参数错误")
	} else if len(args[4]) <= 0 {
		return shim.Error("参数错误")
	} else if len(args[5]) <= 0 {
		return shim.Error("参数错误")
	}

	key := fmt.Sprintf("medical_record_s%_s%_s%_s%_s%_s%", args[0], args[1], args[2], args[3], args[4], args[5])

	// Delete the key from the state in ledger
	err := stub.DelState(key)
	if err != nil {
		fmt.Println("=================delete faild====================")
		return shim.Error("删除状态失败")
	}

	fmt.Println("=================delete success====================")
	return shim.Success(nil)
}

/**
	查询诊疗记录

	arg1: account_no
	arg2: member_id
	arg3: yyyyMMdd
	arg4: clinic_type
	arg5: clinic_hospital
	arg6: clinic_department
 */
func qryMedicalRecord(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	if len(args) != 6 {
		return shim.Error("参数个数不匹配")
	} else if len(args[0]) <= 0 {
		return shim.Error("参数错误")
	} else if len(args[1]) <= 0 {
		return shim.Error("参数错误")
	} else if len(args[2]) <= 0 {
		return shim.Error("参数错误")
	} else if len(args[3]) <= 0 {
		return shim.Error("参数错误")
	} else if len(args[4]) <= 0 {
		return shim.Error("参数错误")
	} else if len(args[5]) <= 0 {
		return shim.Error("参数错误")
	}

	key := fmt.Sprintf("medical_record_s%_s%_s%_s%_s%_s%", args[0], args[1], args[2], args[3], args[4], args[5])

	// Get the state from the ledger
	value, err := stub.GetState(key)
	if err != nil {
		jsonResp := "{\"Error\":\"根据 " + key + "取值失败\"}"
		fmt.Println("===================query faild,err=================")
		return shim.Error(jsonResp)
	}

	if value == nil {
		fmt.Println("===================query faild,vaule nil=================")
		return shim.Error("===================query faild,vaule nil=================")
	}

	fmt.Println("===================query success!!!=================")
	return shim.Success(value)
}

/**
	修改诊疗记录

	arg1: account_no
	arg2: member_id
	arg3: yyyyMMdd
	arg4: clinic_type
	arg5: clinic_hospital
	arg6: clinic_department
	arg7: struct
 */
func modMedicalRecord(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	if len(args) != 7 {
		return shim.Error("参数个数不匹配")
	} else if len(args[0]) <= 0 {
		return shim.Error("参数错误")
	} else if len(args[1]) <= 0 {
		return shim.Error("参数错误")
	} else if len(args[2]) <= 0 {
		return shim.Error("参数错误")
	} else if len(args[3]) <= 0 {
		return shim.Error("参数错误")
	} else if len(args[4]) <= 0 {
		return shim.Error("参数错误")
	} else if len(args[5]) <= 0 {
		return shim.Error("参数错误")
	} else if len(args[6]) <= 0 {
		return shim.Error("参数错误")
	}


	key := fmt.Sprintf("medical_record_s%_s%_s%_s%_s%_s%", args[0], args[1], args[2], args[3], args[4], args[5])
	value := args[6]
	err := stub.PutState(key, []byte(value))
	if err != nil {
		fmt.Println("==============update faild====================")
		return shim.Error(err.Error())
	}
	fmt.Println("==============update success====================")
	return shim.Success(nil)

}
