package main

const errorIncorrectParamNum = "CC_ERROR000001: Incorrect number of parameter."
const errorMissParamNum = "CC_ERROR000002: Miss required parameter."
const errorInvalidFunction = "CC_ERROR000004: Invalid function."
const errorDataAlreadyExist = "CC_ERROR000005: Data already exist."
const errorDataNotExist = "CC_ERROR000006: Data not exist."
const errorBlockchainError = "CC_ERROR000008: Blockchain error."
